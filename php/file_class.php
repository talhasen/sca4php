<!--file_class.php-->
<?php
class File{
	static private $temporary_filename;
	static private $filename;
	static private $filepath;
	static private $projectNames=array();
	public function __construct()
    {
		
    }
	public static function get_temporary_filename(){
		return self::$temporary_filename;
	}
	public static function get_filename(){
		return self::$filename;
	}
	public static function get_filepath(){
		return self::$filepath;
	}
	public static function set_temporary_filename($temporary_filename){
		self::$temporary_filename=$temporary_filename;
	}
	public static function set_filename($filename){
		self::$filename=$filename;
		self::$filepath="./projects/".$filename;
	}
	public static function save_upload_file(){//returns a boolean variable regarding if the file is uploaded to the path specified or not.
		if (@move_uploaded_file(self::get_temporary_filename(),self::get_filepath())){ // if saved
			return True;
		}else {// if not
			return False;
		}
			
		 
	}
	public static function ext($file)  {//returns extension of given file
		$file = strtolower(pathinfo($file, PATHINFO_EXTENSION));
		return $file;
	}
	public static function clear_ext($fname)  {//returns extension of given file
		if(substr_compare($fname, '.', 0)){
			$fname=strstr($fname, '.', true);
		}
		return $fname;
	}
	public static function delete_invalid_files($dirpath)  {
		if($diropened = @opendir($dirpath)){
				while (false !== ($readitem = readdir($diropened))) {
					if(is_file($dirpath."/".$readitem) && ($filetype = self::ext($readitem))!="php" && ($filetype = self::ext($readitem))!="html"){
						@unlink($dirpath."/".$readitem);// deletes invalid files
					}elseif (is_dir($dirpath."/".$readitem)) {
						if(($readitem != '.') && ($readitem != '..')){
							self::delete_invalid_files($dirpath."/".$readitem);
						}
					}
				}	
		}
		@closedir($diropened);
	}
	public static function delete_empty_directory($dirpath)  {//returns extension of given file
		$isEmpty=true;
		if($diropened = @opendir($dirpath)){
				while (false !== ($readitem = readdir($diropened))) {
					if(is_dir($dirpath.'/'.$readitem) && ($readitem != '.') && ($readitem != '..') ){
						$isEmpty=false;
						if(self::delete_empty_directory($dirpath.'/'.$readitem)){
							$isEmpty=true;
						}
					}elseif(is_file($dirpath.'/'.$readitem)){
						$isEmpty=false;
					}
				}
				if($isEmpty){
					@closedir($diropened);
					if($dirpath != "./projects"){
						@rmdir($dirpath);
						return true;
					}else{
						return false;
					}
				}else{
					return false;
				}
		}
	}
	public static function delete_directory($dirpath)  {
		if($diropened = @opendir($dirpath)){
				while (false !== ($readitem = readdir($diropened))) {
					if(is_file($dirpath."/".$readitem)){// deletes all files
						@unlink($dirpath."/".$readitem);
					}elseif (is_dir($dirpath."/".$readitem)) {
						if(($readitem != '.') && ($readitem != '..')){
							self::delete_directory($dirpath."/".$readitem);
						}
					}
				}	
		}
		@closedir($diropened);
		self::delete_empty_directory($dirpath);// deletes directories just now emptied
	}
	public static function create_tree($rootfolder){// recursive function returning a treeview of the directory hierarchy
		if($rootfolder=='./projects'){
			$tree='<ul class="treeview">';
			self::delete_invalid_files($rootfolder);
			self::delete_empty_directory($rootfolder);
		}else{
			$tree='<ul class="unorderedlist">';
		}
		
		if($folderopened = @opendir($rootfolder)){
			//if(!(self::delete_empty_directory($rootfolder))){
					while (false !== ($readitem = readdir($folderopened))) {
						if (is_dir($rootfolder."/".$readitem)){//if it is a folder
							if(($readitem != '.') && ($readitem != '..')/*&&($readitem != basename($_SERVER['PHP_SELF']))*/){
								sessionClass::get_session_id() == '' ? ($sessionID=session_id()) : ($sessionID=sessionClass::get_session_id());	
								if($rootfolder=='./projects' ){
										if(self::check_credential($rootfolder."/".$readitem."/credential.php",$sessionID)){//if it is the autorised client
											$tree.='<li class="dir" id="'.$sessionID.'" ><span class="'.$rootfolder."/".$readitem.'">'.sessionClass::remove_addition_name($readitem)."</span>".self::create_tree($rootfolder."/".$readitem)."</li>";
										}
								}else{// runs when subfolders are read
									$tree.='<li class="dir" id="'.$sessionID.'" ><span class="'.$rootfolder."/".$readitem.'">'.$readitem."</span>".self::create_tree($rootfolder."/".$readitem)."</li>";
								}
							}
						}elseif($readitem != 'credential.php'){//if it is a file( allowed extension "php" and "html") and not credential.php.
							$tree.='<li class="file"><span class="'.$rootfolder."/".$readitem.'">'.$readitem."</span></li>";	
						}
					}
			//}
			$tree.='</ul>';
		}
		@closedir($folderopened);
		return $tree;
	}
	public static function extract_zip_to_projects(){
		if(session_id()==''){
			echo session_id();
			if($_FILES){ 
				self::set_temporary_filename($_FILES["fileupload"]["tmp_name"]);
				self::set_filename($_FILES["fileupload"]["name"]);
				if(True==self::save_upload_file()){//saved
					$zip = new ZipArchive;
					$res = $zip->open(self::get_filepath());
					if ($res === TRUE) { 
						$zip->extractTo("./projects/");
						$status=true;
						if(false !== ($rootDirOfProject=$zip->getNameIndex(1))){
							$rootDirOfProject=strstr($rootDirOfProject , "/" , ture);// gets substr backwards from "/" in the string
								if(self::create_credential("./projects/".$rootDirOfProject) == true){
									if(false === @rename("./projects/".$rootDirOfProject,"./projects/".$rootDirOfProject.sessionClass::get_addition_name())){
										$result="You have already loaded that project before. You are not allowed to load a project twice!";
										$status=false;
										self::delete_directory("./projects/".$rootDirOfProject);
									}else{
										$result="File upload successfully Completed";
										$status=true;
									}
									$zip->close();
								}else{
									$zip->close();
									$result="Having error in creating credential for the project! Please reupload it.";
									@unlink(self::get_filepath());
									$status=false;
								}
						}else{
							$result="Having error in reading a directory in the zip file! Please check your zip file's content and reload.";
							$zip->close();
							@unlink(self::get_filepath());
							$status=false;
						}
						if(!$status){
							$result="Your upload process discontinued. :( ".$result;
						}
						return array("result" => $result,"status" => $status);
					}
				}else{
					return array("result" => "You chose no file! First, click on the Browse... button then the Load button","status" => "No uploading");
				}
			}else{// runs if there is a refreshing the index.php page or if the index.php page is opened in a new tap of the browser.
				/*
				 * in here the page refreshed or reloaded the page! To delete the user's before-uploaded projects you can make uncomment this comment block.
				 * 
				$clientCredential=sessionClass::get_session_id();
				if($diropened = @opendir("./projects")){
					while (false !== ($readitem = readdir($diropened))) {
						if(is_dir("./projects/".$readitem) && ($readitem != '.') && ($readitem != '..')){//selects projects' root folders
							if(self::check_credential("./projects/".$readitem."/credential.php",$clientCredential)){// deletes the selected project, if the selected project belongs to the client logged on 
								self::delete_directory("./projects/".$readitem);
							}
						}
					}	
				}
				@closedir($diropened);*/
				return array("result" => "You refreshed or reloaded the page! The projects you have uploaded before have been deleted. Please reupload a project or a few projects to be analysed...","status" => "");
			}
		}
	}
	public static function get_all_projects($rootfolder){
		self::$projectNames= array();
		if($folderopened = @opendir($rootfolder)){
			while (false !== ($readProjectDir = readdir($folderopened))){
				if(is_dir($rootfolder."/".$readProjectDir) && ($readProjectDir != '.') && ($readProjectDir != '..')){
					array_push(self::$projectNames, $readProjectDir);
				}
			}	
		}
		@closedir($folderopened);
		return self::$projectNames;
	}
	public static function get_all_my_projects($rootfolder){
		self::$projectNames=array();
		if($folderopened = @opendir($rootfolder)){
			while (false !== ($readProjectDir = readdir($folderopened))){
				if(is_dir($rootfolder."/".$readProjectDir) && ($readProjectDir != '.') && ($readProjectDir != '..')){
					$fileopened = @fopen($rootfolder."/".$readProjectDir."/credential.php", "r");
					$content = @stream_get_contents($fileopened);
					@fclose($fileopened);
					if($content==sessionClass::get_session_id()){
						array_push(self::$projectNames, $readProjectDir);
					}
				}
			}	
		}
		@closedir($folderopened);
		return self::$projectNames;
	}
	public static function create_credential($credentialPath){
			/*if(false !==*/ $fileCreated = @fopen($credentialPath.'/credential.php', "w+");/*){*/
				if(false!==@fwrite($fileCreated, sessionClass::get_session_id())){
					@fclose($fileCreated);
					return true;
				}
			//}
			@fclose($fileCreated);
			return false;
	}
	public static function check_credential($credentialPath,$credentialToBeCompared){// returns 1(true) or 0(false) depending on whether there is no uploaded project or not
		$file=@fopen($credentialPath,'r');
		$credential=@stream_get_contents($file);
		//$credential=@fread($file, filesize($$credentialPath));
		@fclose($file);
		if($credential==$credentialToBeCompared){
			return true;
		}else{
			return false;
		}
	}
	public static function exists_credential($credentialPath){// returns 1(true) or 0(false) depending on whether there is no uploaded project or not
		$file=@fopen($credentialPath,'r');
		$credential=@fread($file, filesize($file));
		@fclose($file);
		if($credential==$credentialToBeCompared){
			return true;
		}else{
			return false;
		}
	}

}
?>

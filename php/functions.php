<?php
@include("session_class.php");// this is needed to get addition_name for makeRealPath($arg) method.
function makeRealPath($path) {
	$projecName=strstr($path,'/',true);
	$projecRealName=$projecName.sessionClass::get_addition_name();
	$realPath="../projects/".$projecRealName.strstr($path,'/');
	return $realPath;
}
function ignoreBlocks($line,$start,$end){//removes any html or any script block such as <script>...</script>, <html>...</html> ,<?php ... and so on.
    if(stristr($line, $start) !== false) {
        $scriptExists=true;
        $line=strstr($line , $start , true);// gets substr backwards from "<script" in the line
        if(stristr($line, $end) !== false){//runs if both "<script" and "</script" are in the same line; gets substr forward from "</script" in the line
          $scriptExists=false;
          $line=strstr($line , $end);
        }
      }elseif(stristr($line, $end) !== false){
        $scriptExists=false;
        $line=strstr($line, $end);// gets substr forward from "</script" in the line
      }
      return array($line,$scriptExists);
}
?>
<?php 
class codeviewClass {
	private $filename;
	private $filepath;
	private $lineNo;
	private $totalline;
	function __construct($post_arr) {
		if($post_arr["filename"]){
			$this->filename=$post_arr["filename"];
			$this->filepath=$post_arr["filepath"];
			$this->show_whole_file();
		}elseif($post_arr["linenumber"]){
			$this->lineNo=$post_arr["linenumber"];
			$this->filepath=makeRealPath($post_arr["filepath"]);
			$this->totalline=$post_arr["totalline"];
			$this->show_vulnerability_part_only();
		}
	}
	function show_whole_file() {
		$file=@fopen("../".$this->filepath,'r');
		$lineNum=0;
		while(!feof($file)){
			$lineNum++;
			$line=@fgets($file);
			if($lineNum==1){
				$lines.="    ".$line;
			}else{
				$lines.=" ".$lineNum."  ".$line;
			}
		}
		fclose($file);

		echo '<hr class="codeViewHrFirst"><ul id="codeUL" class="treeview"><li class="file">'.$this->filename.'</li><hr class="codeViewHr">';
		echo '<li class="codeLI"><div id="content-code-block" >'.highlight_string($lines,true).'</div></li></ul>';
	}
	function show_vulnerability_part_only() {
		$linecontent='';
		$contentArray=file($this->filepath);
		if($this->totalline==1){
			$linenumbers = implode(range($this->lineNo-1,$this->lineNo+1), '<br/>');
		}else{
			$linenumbers = implode(array($this->lineNo-1,$this->lineNo,$this->lineNo+$this->totalline), '<br/>');
		}
		$content='';
		echo '<style type="text/css">
			        .num {
			        float: left;
			        color: #181A1C;
			        font-size: 14px;
			        font-family: monospace;
			        text-align: right;
			        margin-right: 6pt;
			        padding-right: 6pt;
			        border-right: 2px solid #181A1C;}'
				//body {margin: 0px; margin-left: 5px;}
		.'td {vertical-align: top;}
			        code {white-space: nowrap;}
			    </style>';
		echo "<table><tr><td class=\"num\">\n$linenumbers\n</td><td>\n";
		foreach ($contentArray as $line_num => $line) {
			$number=$line_num+1;
			if($number==($this->lineNo-1)  || $number==($this->lineNo+$this->totalline)){
				echo highlight_string($line,true);
			}elseif($number==$this->lineNo){
				if($this->totalline!=1){
					$obj=new analysis("not importtant");// just to use this method "removeUnneccessarySpaces" belonging to the analysis class
					foreach ($contentArray as $key => $value) {
						if($key>=$line_num && $key<($line_num+$this->totalline)){
							$linecontent.=$value;
							$oneLine.=$obj->removeUnneccessarySpaces($value,$key);
						}
					}
					echo highlight_string($oneLine,true);
					echo "<br>";
				}else{
					echo highlight_string($line,true);
				}
			}
		}
		echo "\n</td></tr></table>";
	}
}
?>
<?php
/**
* 
*/
class sessionClass 
{
	private static $sessionID;
	function __construct()
	{

	}
	static function  check_and_setCookie(){
		if(!isset($_COOKIE["tmpCookie"])){
			session_start();
			session_regenerate_id();
			self::$sessionID=session_id();
			setcookie("tmpCookie",session_id());
		}
	}
	static function  get_session_id(){
		return $_COOKIE["tmpCookie"];
	}
	static function get_addition_name(){
		if(sessionClass::get_session_id() == ''){
			return "_".substr(session_id(), 0, 15);//gets first 15 characters with substr and adds "_" separator.
		}else{
			return "_".substr(self::get_session_id(), 0, 15);//gets first 15 characters with substr and adds "_" separator.
		}
	}
	static function remove_addition_name($fullName){
		//return strstr($fullName, '_',true);;//removes the substr after '_' from $fullName including '_' character
		if(false === $pos=strrpos($fullName, "_")){
			return $fullName;
		}
		return substr($fullName,0,$pos);
	}
	
}
	
	
?>

<?php 
@require('../fpdf_lib/fpdf.php');// contains pdf class, fonts.
//require 'session_class.php';//because function.php contains session_class.php, it is made comment line
@require("functions.php");
if($_POST){
	$obj = new pdfClass();
}
class pdfClass {
	private $vulCountsArr=array();
	private $flawtypeArr=array();
	function __construct() {
		@mkdir("../reports/".sessionClass::get_addition_name(), 0700);//creates a report directory per user.
		$this->create_pdfs_per_project($_POST,$_POST["analysis"]);
	}
	function create_pdfs_per_project($post_arr, $analysis_arr){
		foreach ($analysis_arr as $key => $value) {
			$totalcount=0;
			$tempflawtypeArr=array();
			$tempVulCountArr=array();
			foreach ($value as $subkey1 => $subvalue1) {
				foreach ($subvalue1 as $subkey2 => $subvalue2) {
					if($subkey2 != 'project_name'){// means only vulnerabilities
						//--------stacking all flawtypes*start--------
						if(false === in_array($subvalue2["flawtype"], $tempflawtypeArr)){
							array_push($tempflawtypeArr, $subvalue2["flawtype"]);
						}
						//--------stacking all flawtypes*end--------
						//--------grouping vulmessage arrays according to their belonging to which project and owning which flawtype*start--------
						$tempArr=$key.$subvalue2["flawtype"];
						if(is_array($$tempArr)){//creating arrays as total flawtypes as, and add its items according to item's flawtype
							array_push($$tempArr,$subvalue2);//add the $subvalue2 array as an item of the $$subvalue2["flawtype"] array
						}else{
							$$tempArr=array();//creates an empty array with the name $tempArr ($tempArr=$key.$subvalue2["flawtype"])
							array_push($$tempArr,$subvalue2);//adds the $subvalue2 array as an item of the $$subvalue2["flawtype"] array
						}
						//--------grouping vulmessage arrays according to their belonging to which project and owning which flawtype*end-------
						foreach ($subvalue2 as $subkey3 => $subvalue3) {
							if($subkey3=='flawtype'){
								$name=$key.$subvalue3."count";
								if(false===isset($$name)){// if $$name does not exist
									$$name=0;
								}
								$$name+=1;
								$totalcount++;
							}
						}
		
					}
				}
			}
			foreach ($tempflawtypeArr as $flawkey => $flawvalue) {//generates a variable with a value depending on the flawtype of foreach loop and project no, this var's value is also another variable involving the depended flawtype's count in the project specified in its name.
				$flawVarName=$key.$flawvalue."count";
				$tempVulCountArr[$flawVarName]=$$flawVarName;
			}
			$tempVulCountArr["total"]=$totalcount;
			array_push($this->vulCountsArr,$tempVulCountArr);
			array_push($this->flawtypeArr, $tempflawtypeArr);
		}
		//----calculation of percentage*start----
		foreach ($this->vulCountsArr as $key => $value) {
			foreach ($value as $key2 => $value2) {
				if(false===strpos($key2,"percentage") && $key2!="total"){
					$this->vulCountsArr[$key]["percentageOf".$key2]=round(($value2*100)/$value["total"], 2);
				}
			}
		}
		//----calculation of percentage*end----
		foreach ($post_arr as $key => $value){
			if($key !== "analysis"){// as much as project count
				@$pdf = new FPDF();
				@$pdf->AddPage();
				@$pdf->SetFont('Arial','B',16);
				@$pdf->Cell(1);
				@$pdf->Cell(0,10,strtoupper($value).' SECURITY REPORT',0,1,'C');// caption of the document
				@$pdf->Cell(0,10,'',0,1);//just written to adjust text
				foreach ($this->flawtypeArr as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						if($key==$key2){
							$count=0;
							@$pdf->Cell(0,1,'',0,1);//y-1px empty line
							@$pdf->SetFont('Arial','B',10);
							$remainingY=(@$pdf->GetPageHeight())-(@$pdf->GetY());
							if($remainingY<43){
								@$pdf->AddPage('P','A4');
							}
							@$pdf->Cell(62,5,'Vulnerability Type : '.$value3,'B',0,'L');
							$subkeyname=$key2.$value3."count";
							$subpercentagename="percentageOf".$key2.$value3."count";
							@$pdf->Cell(43,5,' Amount : '.$this->vulCountsArr[$key][$subkeyname],'B',0);
							switch ($value3) {
								case Warning:
									@$pdf->Cell(57,5,' Severity : Medium','B',0);
									$pieChartName='Ypie';// Y means Yellow representing Medium and Warning
									break;
								case SQLi:
									@$pdf->Cell(57,5,' Severity : High','B',0);
									$pieChartName='Opie';// O means Orange representing High
									break;
								case XSS:
									@$pdf->Cell(57,5,' Severity : High','B',0);
									$pieChartName='Opie';// O means Orange representing High
									break;
								case LFI:
									@$pdf->Cell(57,5,' Severity : High','B',0);
									$pieChartName='Opie';// O means Orange representing High
									break;
								case RFI:
									@$pdf->Cell(57,5,' Severity : Critical','B',0);
									$pieChartName='Rpie';// R means Red representing Critical
									break;
								case CI:
									@$pdf->Cell(57,5,' Severity : Critical','B',0);
									$pieChartName='Rpie';// R means Red representing Critical
									break;
								default:
									break;
							}
							//-----
							for ($item = 0; $item < 100; $item=$item+5) {
								if($this->vulCountsArr[$key][$subpercentagename]<$item){
									$pieChartName.=$item;// adds pie chart's name its percentage
									break;
								}
							}
							//-----
							@$pdf->Cell(0,5,'Percentage','B',1);
							@$pdf->Image("../images/ico/$pieChartName.png",168,@$pdf->GetY()+3,30,30,'PNG');
							@$pdf->SetFont('Arial','BI',7);
							@$pdf->Cell(0,2,'',0,1);//y-1px empty line
							@$pdf->Cell(176.5,1,$this->vulCountsArr[$key][$subpercentagename].'%',0,1,'R');
							@$pdf->SetFont('Arial','',10);
							$vulMessageArrName=$key2.$value3;// the array name owning all the "$value3`s" flawtype`s vulnerability message array
							foreach ($$vulMessageArrName as $key4 => $value4) {
								//---------showing vul line*start--------------
								@$pdf->SetFont('Arial','',10);
								$filepath=makeRealPath($value4["path"]);
								$file=@fopen($filepath,'r');
								$lineNum=0;
								//while(!feof($file)){
								//$lineNum++;
								$line=$value4["linecontent"];
								//if($lineNum==$value4["line"]){//finds the line containing the vulnerability
								if(strlen($line)>83){
									$line=substr($line,0,83)."...";
								}
								switch ($value4["flawtype"]) {
									case Warning:
										@$pdf->SetFillColor(255,201,14);//makes it yellow
										break;
									case SQLi:
										@$pdf->SetFillColor(255,127,39);//makes it orange
										break;
									case XSS:
										@$pdf->SetFillColor(255,127,39);//makes it orange
										break;
									case LFI:
										@$pdf->SetFillColor(255,127,39);//makes it orange
										break;
									case RFI:
										@$pdf->SetFillColor(237,28,36);//makes it red
										break;
									case CI:
										@$pdf->SetFillColor(237,28,36);//makes it red
										break;
									default:
										break;
								}
								@$pdf->Cell(1,5,' ',0,0);//x-1px space
								@$pdf->Cell(155,5,$value4["line"]." ".$line,'TRL',1,'L',true);
								$count++;
								//}
								//}
								@fclose($file);
								//---------showing vul line*end----------------
								@$pdf->SetFont('Arial','I',10);
								if(strlen($line)>83){
									$path=substr($value4["path"],0,83)."...";
								}else{
									$path=$value4["path"];
								}
								@$pdf->Cell(1,5,' ',0,0);//x-1px space
								@$pdf->Cell(155,5,"in ".$path,'BRL',1);
								@$pdf->Cell(0,1,'',0,1);//y-1px empty line
							}
							if($count>3){
								@$pdf->Rect(@$pdf->GetX(),@$pdf->GetY()-$count*11-10,190,43+($count-3)*11);
							}else{
								@$pdf->Rect(@$pdf->GetX(),@$pdf->GetY()-($count)*11-10,190,43);
							}
							if($count >= 3){
								@$pdf->Cell(0,3,'',0,1);//y-5px empty line

							}elseif($count == 2){
								@$pdf->Cell(0,14,'',0,1);//y-15px empty line
							}elseif($count == 1){
								@$pdf->Cell(0,25,'',0,1);//y-10px empty line
							}
						}

					}
				}
				@$pdf->Output("../reports/".sessionClass::get_addition_name()."/".$value.".pdf","F");//saves the document under the path.
			}
		}
	}
}
?>
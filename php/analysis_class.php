<?php
	/**
	* 
	*/
	class analysis
	{
		private $projectpath;
		private $projectname;
		function __construct($projectpath)
		{
			$this->projectpath=$projectpath;
			$this->set_project_name($projectpath);
			
		}
		function get_project_name(){
			return $this->projectname;
		}
		function set_project_name($path){
			$path = substr($path,strrpos($path,"/")+1);
			$this->projectname=sessionClass::remove_addition_name($path);
		}
		function prepare_project_path_to_show($path){
			$rightSide = substr($path,strpos($path,"_"));
			$rightSide = substr($rightSide,strpos($rightSide,"/"));
			return $this->get_project_name().$rightSide;
		}
		function analyse(){
			$fullResponseArray=array();
			array_push($fullResponseArray,$this->SQLi_XSS_FI($this->projectpath));
			//array_push($fullResponseArray,$this->another_function($this->projectpath));//for another function
			return $fullResponseArray;
		}
		function SQLi_XSS_FI($projectpath){//more than one types of SqlI such as blind, error-based, etc.
			$responseArray=array("project_name" => $this->get_project_name());
			//array_push($responseArray,$this->get_project_name());
			if($diropened = @opendir($projectpath)){
				while (false !== ($readitem = @readdir($diropened))) {
					if(is_dir($projectpath."/".$readitem) && ($readitem != '.') && ($readitem != '..')){// if there is a subfolder, goes into it
						$subResponseArray=$this->SQLi_XSS_FI($projectpath."/".$readitem);
						$responseArray=array_merge($responseArray,$subResponseArray);
					}elseif(is_file($projectpath."/".$readitem)){
						$fileArray = @file($projectpath."/".$readitem);
						$fileOpened = @fopen($projectpath."/".$readitem, "r");
						$contentOfFile = @fread($fileOpened, @filesize($projectpath."/".$readitem));
						@fclose($fileOpened);
						//-------------------for POST -----------------------------------
						$varsPOST4SQLi=array();
						$varsPOST4XSS=array();
						$varsPOST4FI=array();
						$varsPOST4CmdExec=array();
						$varsPOST4RCE=array();
						array_push($varsPOST4SQLi,'$_POST');
						array_push($varsPOST4XSS,'$_POST');
						array_push($varsPOST4FI,'$_POST');
						array_push($varsPOST4CmdExec,'$_POST');
						array_push($varsPOST4RCE,'$_POST');
						if(false!==strpos($contentOfFile,'$_POST[')){
								foreach ($fileArray as $line_num => $line) {
									//add variable if there is an assignment $_POST to a variable
									$next_line=$line_num + 1;
									//******SQLi*****
									$varsPOST4SQLi=$this->findAssignedVar($line,$varsPOST4SQLi,'SQLi');
									if(false!==stripos($line,'select ') || false!==stripos($line,'update ') || false!==stripos($line,'delete ') || false!==stripos($line,'insert into ') || false!==stripos($line,'create or') || false!==stripos($line,'create view  ') || false!==stripos($line,'create table ') || false!==stripos($line,'create database ') || false!==stripos($line,'drop table') || false!==stripos($line,'drop database') || false!==stripos($line,'drop view ') || false!==stripos($line,'create index ') || false!==stripos($line,'create unique index ') || false!==stripos($line,'drop index ') || false!==stripos($line,'alter table ') || false!==stripos($line,'truncate table ')  ){
										if(false !== ($MultipleLines=$this->isMultipleLine($fileArray, $line)) ){// if there is a command line consisting of multiple row.
											foreach ($varsPOST4SQLi as $index => $value) {
												foreach ($MultipleLines as $MLindex => $MLvalue) {
													$linecontent4POSTSQLi.=$this->removeUnneccessarySpaces($MLvalue, $MLindex);
													if(false===strpos($MLvalue,'filter_var') && false===strpos($MLvalue,'filter_var_array') && false===strpos($line,'array_filter') && (false===strpos($line,'filter_var') && false===strpos($line,'FILTER_CALLBACK') ) ){
														if($this->compare($MLvalue,$value)){
															array_push($responseArray,array("method" => "POST","flawtype" => "SQLi","line" => $next_line,"totalline" => $MLindex+1,"linecontent" => $linecontent4POSTSQLi ,"path" => $this->prepare_project_path_to_show($projectpath."/".$readitem)));
															$linecontent4POSTSQLi='';
															break;//leave the if statements and also the foreach loop //keep in mind that "break 2;" leaves two loop
														}
														
													}
												}
												$linecontent4POSTSQLi='';
											}
										}else{// if there is a command line consisting of only one row.
											foreach($varsPOST4SQLi as $index => $value){
												if($this->compare($line,$value)){
													if(false===strpos($line,'filter_var') && false===strpos($line,'filter_var_array') && false===strpos($line,'array_filter') && (false===strpos($line,'filter_var') && false===strpos($line,'FILTER_CALLBACK') ) ){
														array_push($responseArray,array("method" => "POST","flawtype" => "SQLi","line" => $next_line,"totalline" => 1,"linecontent" => $line,"path" => $this->prepare_project_path_to_show($projectpath."/".$readitem)));
														break;//leave the if statement and also the foreach loop //keep in mind that "break 2;" leaves two loop
													}
												}
											}
										}
										
									}
									//******SQLi-end*****
									//******XSS*****
									$varsPOST4XSS=$this->findAssignedVar($line,$varsPOST4XSS,'XSS');
									if(false!==stripos($line,'echo ') || false!==stripos($line,'print') /*|| false!==stripos($line,'print_r') || false!==stripos($line,'printf') || false!==stripos($line,'vprintf')*/ ){
										if(false !== ($MultipleLines=$this->isMultipleLine($fileArray, $line)) ){// if there is a command line consisting of multiple row.
											foreach ($varsPOST4XSS as $index => $value) {
												foreach ($MultipleLines as $MLindex => $MLvalue) {
													$linecontent4POSTXSS.=$this->removeUnneccessarySpaces($MLvalue, $MLindex);
													if(false===strpos($line,'filter_var') && false===strpos($line,'filter_var_array') && false === strpos($line,'htmlspecialchars') &&  false === strpos($line,'htmlentities') &&  false === strpos($line,'highlight_string') &&  false === strpos($line,'strip_tags') && false===strpos($line,'array_filter') && (false===strpos($line,'filter_var') && false===strpos($line,'FILTER_CALLBACK') ) ){
														if($this->compare($MLvalue,$value)){
															array_push($responseArray,array("method" => "POST","flawtype" => "XSS","line" => $next_line,"totalline" => $MLindex+1,"linecontent" => $linecontent4POSTXSS ,"path" => $this->prepare_project_path_to_show($projectpath."/".$readitem)));
															$linecontent4POSTXSS='';
															break;//leave the if statements and also the foreach loop //keep in mind that "break 2;" leaves two loop
														}
													}
												}
												$linecontent4POSTXSS='';
											}
										}else{// if there is a command line consisting of only one row.
											foreach($varsPOST4XSS as $index => $value){
												if($this->compare($line,$value)){
													if(false===strpos($line,'filter_var') && false===strpos($line,'filter_var_array') && false === strpos($line,'htmlspecialchars') &&  false === strpos($line,'htmlentities') &&  false === strpos($line,'highlight_string') &&  false === strpos($line,'strip_tags') && false===strpos($line,'array_filter') && (false===strpos($line,'filter_var') && false===strpos($line,'FILTER_CALLBACK') ) ){
														array_push($responseArray,array("method" => "POST","flawtype" => "XSS","line" => $next_line,"totalline" => 1,"linecontent" => $line,"path" => $this->prepare_project_path_to_show($projectpath."/".$readitem)));
														break;
													}
												
												}
											}
										}
									}
									//******XSS-end*****
									//******warning*****
									if(false!==stripos($line,'return ') ){
										foreach($varsPOST4XSS as $index => $value){
											if($this->compare($line,$value)){
												array_push($responseArray,array("method" => "POST","flawtype" => "Warning","line" => $next_line,"totalline" => 1,"linecontent" => $line,"path" => $this->prepare_project_path_to_show($projectpath."/".$readitem)));
												break;
											}
										}
									}
									//******warning-end*****
									//******FI*****
									$varsPOST4FI=$this->findAssignedVar($line,$varsPOST4FI,'FI');
									if( false!==stripos($line,'include') || false!==stripos($line,'include_once') || false!==stripos($line,'require') || false!==stripos($line,'require_once') ){
										if(false !== ($MultipleLines=$this->isMultipleLine($fileArray, $line)) ){// if there is a command line consisting of multiple row.
											foreach ($varsPOST4FI as $index => $value) {
												foreach ($MultipleLines as $MLindex => $MLvalue) {
													$linecontent4POSTFI.=$this->removeUnneccessarySpaces($MLvalue, $MLindex);
													if( (false===strpos($line,'filter_var') && false===strpos($line,'FILTER_CALLBACK') ) && ( false===strpos($line,'filter_var_array') && false===strpos($line,'FILTER_CALLBACK') ) && ( false===strpos($line,'explode') && false===strpos($line,'"/') ) && false===strpos($line,'array_filter') ){
														if($this->compare($MLvalue,$value)){
															if($this->isRFI($line)){//RFI
																array_push($responseArray,array("method" => "POST","flawtype" => "RFI","line" => $next_line,"totalline" => $MLindex+1,"linecontent" => $linecontent4POSTFI,"path" => $this->prepare_project_path_to_show($projectpath."/".$readitem)));
															}else{//LFI
																array_push($responseArray,array("method" => "POST","flawtype" => "LFI","line" => $next_line,"totalline" => $MLindex+1,"linecontent" => $linecontent4POSTFI,"path" => $this->prepare_project_path_to_show($projectpath."/".$readitem)));
															}
															$linecontent4POSTFI='';
															break;//leave the if statements and also the foreach loop //keep in mind that "break 2;" leaves two loop
														}
										
													}
												}
												$linecontent4POSTFI='';
											}
										}else{// if there is a command line consisting of only one row.
											foreach($varsPOST4FI as $index => $value){
												if($this->compare($line,$value)){
													if( (false===strpos($line,'filter_var') && false===strpos($line,'FILTER_CALLBACK') ) && ( false===strpos($line,'filter_var_array') && false===strpos($line,'FILTER_CALLBACK') ) && ( false===strpos($line,'explode') && false===strpos($line,'"/') ) && false===strpos($line,'array_filter') ){
														if($this->isRFI($line)){//RFI
															array_push($responseArray,array("method" => "POST","flawtype" => "RFI","line" => $next_line,"totalline" => 1,"linecontent" => $line,"path" => $this->prepare_project_path_to_show($projectpath."/".$readitem)));
														}else{//LFI
															array_push($responseArray,array("method" => "POST","flawtype" => "LFI","line" => $next_line,"totalline" => 1,"linecontent" => $line,"path" => $this->prepare_project_path_to_show($projectpath."/".$readitem)));
														}
														break;
													}
												}
											}
											
										}
										// is there a never-assigned variable in this line? if so, it is register global vulnerability and highly likely may cause RLF or LFI. use isRFI()
									}
									//******FI-end*****
									//******CI*****
									$varsPOST4CmdExec=$this->findAssignedVar($line,$varsPOST4CmdExec,'CmdExec');
									if(false!==stripos($line,'exec') || false!==stripos($line,'shell_exec')  || false!==stripos($line,'system') || false!==stripos($line,'passthru') || false!==stripos($line,'proc_open') || false!==stripos($line,'popen')   ){
										if(false !== ($MultipleLines=$this->isMultipleLine($fileArray, $line)) ){// if there is a command line consisting of multiple row.
											foreach ($varsPOST4CmdExec as $index => $value) {
												foreach ($MultipleLines as $MLindex => $MLvalue) {
													$linecontent4POSTCmdExec.=$this->removeUnneccessarySpaces($MLvalue, $MLindex);
													if(false===strpos($line,'escapeshellarg') && false===strpos($line,'escapeshellcmd') && false===strpos($line,'array_filter') && (false===strpos($line,'filter_var') && false===strpos($line,'FILTER_CALLBACK') ) ){
														if($this->compare($MLvalue,$value)){
															array_push($responseArray,array("method" => "POST","flawtype" => "XSS","line" => $next_line,"totalline" => $MLindex+1,"linecontent" => $linecontent4POSTCmdExec ,"path" => $this->prepare_project_path_to_show($projectpath."/".$readitem)));
															$linecontent4POSTCmdExec='';
															break;//leave the if statements and also the foreach loop //keep in mind that "break 2;" leaves two loop
														}
										
													}
												}
												$linecontent4POSTCmdExec='';
											}
										}else{// if there is a command line consisting of only one row.
											foreach($varsPOST4CmdExec as $index => $value){
												if($this->compare($line,$value)){
													if(false===strpos($line,'escapeshellarg') && false===strpos($line,'escapeshellcmd') && false===strpos($line,'array_filter') && (false===strpos($line,'filter_var') && false===strpos($line,'FILTER_CALLBACK') ) ){
														array_push($responseArray,array("method" => "POST","flawtype" => "CI","line" => $next_line,"totalline" => 1,"linecontent" => $line,"path" => $this->prepare_project_path_to_show($projectpath."/".$readitem)));
														break;
													}
														
												}
											}
										}
									}
									//******CI-end*****
								
								}
						}
						//-----------for GET--------------------------------------
						$varsGET4SQLi=array();
						$varsGET4XSS=array();
						$varsGET4FI=array();
						$varsGET4CmdExec=array();
						array_push($varsGET4SQLi,'$_GET');
						array_push($varsGET4XSS,'$_GET');
						array_push($varsGET4FI,'$_GET');
						array_push($varsGET4CmdExec,'$_GET');
						if(false!==strpos($contentOfFile,'$_GET[')){
								foreach ($fileArray as $line_num => $line) {
									$next_line=$line_num + 1;
									//******SQLi*****
									$varsGET4SQLi=$this->findAssignedVar($line,$varsGET4SQLi,'SQLi');
									if(false!==stripos($line,'select ') || false!==stripos($line,'update ') || false!==stripos($line,'delete ') || false!==stripos($line,'insert into ') || false!==stripos($line,'create or') || false!==stripos($line,'create view  ') || false!==stripos($line,'create table ') || false!==stripos($line,'create database ') || false!==stripos($line,'drop table') || false!==stripos($line,'drop database') || false!==stripos($line,'drop view ') || false!==stripos($line,'create index ') || false!==stripos($line,'create unique index ') || false!==stripos($line,'drop index ') || false!==stripos($line,'alter table ') || false!==stripos($line,'truncate table ')  ){
										if(false !== ($MultipleLines=$this->isMultipleLine($fileArray, $line)) ){// if there is a command line consisting of multiple row.
											foreach ($varsGET4SQLi as $index => $value) {
												foreach ($MultipleLines as $MLindex => $MLvalue) {
													$linecontent4GETSQLi.=$this->removeUnneccessarySpaces($MLvalue, $MLindex);
													if(false===strpos($MLvalue,'filter_var') && false===strpos($MLvalue,'filter_var_array') && false===strpos($line,'array_filter') && (false===strpos($line,'filter_var') && false===strpos($line,'FILTER_CALLBACK') ) ){
														if($this->compare($MLvalue,$value)){
															array_push($responseArray,array("method" => "GET","flawtype" => "SQLi","line" => $next_line, "totalline" => $MLindex+1, "linecontent" => $linecontent4GETSQLi ,"path" => $this->prepare_project_path_to_show($projectpath."/".$readitem)));
															$linecontent4GETSQLi='';
															break;//leave the if statements and also the foreach loop //keep in mind that "break 2;" leaves two loop
														}
										
													}
												}
												$linecontent4GETSQLi='';
											}
										}else{// if there is a command line consisting of only one row.
											foreach($varsGET4SQLi as $index => $value){
												if($this->compare($line,$value)){
													if(false===strpos($line,'filter_var') && false===strpos($line,'filter_var_array') && false===strpos($line,'array_filter') && (false===strpos($line,'filter_var') && false===strpos($line,'FILTER_CALLBACK') ) ){
														array_push($responseArray,array("method" => "GET","flawtype" => "SQLi","line" => $next_line,"totalline" => 1,"linecontent" => $line,"path" => $this->prepare_project_path_to_show($projectpath."/".$readitem)));
														break;
													}
												}
											}
										}
									}
									//******SQLi-end*****
									//******XSS*****
									$varsGET4XSS=$this->findAssignedVar($line,$varsGET4XSS,'XSS');
									if(false!==stripos($line,'echo') || false!==stripos($line,'print') ){
										if(false !== ($MultipleLines=$this->isMultipleLine($fileArray, $line)) ){// if there is a command line consisting of multiple row.
											foreach ($varsGET4XSS as $index => $value) {
												foreach ($MultipleLines as $MLindex => $MLvalue) {
													$linecontent4GETXSS.=$this->removeUnneccessarySpaces($MLvalue, $MLindex);
													if(false===strpos($line,'filter_var') && false===strpos($line,'filter_var_array') && false === strpos($line,'htmlspecialchars') &&  false === strpos($line,'htmlentities') &&  false === strpos($line,'highlight_string') &&  false === strpos($line,'strip_tags') && false===strpos($line,'array_filter') && (false===strpos($line,'filter_var') && false===strpos($line,'FILTER_CALLBACK') ) ){
														if($this->compare($MLvalue,$value)){
															array_push($responseArray,array("method" => "GET","flawtype" => "XSS","line" => $next_line,"totalline" => $MLindex+1,"linecontent" => $linecontent4GETXSS ,"path" => $this->prepare_project_path_to_show($projectpath."/".$readitem)));
															$linecontent4GETXSS='';
															break;//leave the if statements and also the foreach loop //keep in mind that "break 2;" leaves two loop
														}
										
													}
												}
												$linecontent4GETXSS='';
											}
										}else{// if there is a command line consisting of only one row.
											foreach($varsGET4XSS as $index => $value){
												if($this->compare($line,$value)){
													if(false===strpos($line,'filter_var') && false===strpos($line,'filter_var_array') && false === strpos($line,'htmlspecialchars') &&  false === strpos($line,'htmlentities') &&  false === strpos($line,'highlight_string') &&  false === strpos($line,'strip_tags') && false===strpos($line,'array_filter') && (false===strpos($line,'filter_var') && false===strpos($line,'FILTER_CALLBACK') ) ){
														array_push($responseArray,array("method" => "GET","flawtype" => "XSS","line" => $next_line,"totalline" => 1,"linecontent" => $line,"path" => $this->prepare_project_path_to_show($projectpath."/".$readitem)));
														break;
													}
												}
											}
										}
									}
									//******XSS-end*****
									//******Warning*****
									if(false!==stripos($line,'return ') ){
										foreach($varsGET4XSS as $index => $value){
											if($this->compare($line,$value)){
												array_push($responseArray,array("method" => "GET","flawtype" => "Warning","line" => $next_line,"totalline" => 1,"linecontent" => $line,"path" => $this->prepare_project_path_to_show($projectpath."/".$readitem)));
												break;
											}
										}
									}
									//******Warning-end*****
									//******FI*****
									$varsGET4FI=$this->findAssignedVar($line,$varsGET4FI,'FI');
									if(false!==stripos($line,'include') || false!==stripos($line,'include_once') || false!==stripos($line,'require') || false!==stripos($line,'require_once') ){
										if(false !== ($MultipleLines=$this->isMultipleLine($fileArray, $line)) ){// if there is a command line consisting of multiple row.
											foreach ($varsGET4FI as $index => $value) {
												foreach ($MultipleLines as $MLindex => $MLvalue) {
													$linecontent4GETFI.=$this->removeUnneccessarySpaces($MLvalue, $MLindex);
													if( (false===strpos($line,'filter_var') && false===strpos($line,'FILTER_CALLBACK') ) && ( false===strpos($line,'filter_var_array') && false===strpos($line,'FILTER_CALLBACK') ) && ( false===strpos($line,'explode') && false===strpos($line,'"/') ) && false===strpos($line,'array_filter') ){
														if($this->compare($MLvalue,$value)){
															if($this->isRFI($line)){//RFI
																array_push($responseArray,array("method" => "GET","flawtype" => "RFI","line" => $next_line,"totalline" => $MLindex+1,"linecontent" => $linecontent4GETFI,"path" => $this->prepare_project_path_to_show($projectpath."/".$readitem)));
															}else{//LFI
																array_push($responseArray,array("method" => "GET","flawtype" => "LFI","line" => $next_line,"totalline" => $MLindex+1,"linecontent" => $linecontent4GETFI,"path" => $this->prepare_project_path_to_show($projectpath."/".$readitem)));
															}
															$linecontent4GETFI='';
															break;//leave the if statements and also the foreach loop //keep in mind that "break 2;" leaves two loop
														}
										
													}
												}
												$linecontent4GETFI='';
											}
										}else{// if there is a command line consisting of only one row.
											foreach($varsGET4FI as $index => $value){
												if($this->compare($line,$value)){
													if( (false===strpos($line,'filter_var') && false===strpos($line,'FILTER_CALLBACK') ) && ( false===strpos($line,'filter_var_array') && false===strpos($line,'FILTER_CALLBACK') ) && ( false===strpos($line,'explode') && false===strpos($line,'"/') ) && false===strpos($line,'array_filter') ){
														if($this->isRFI($line)){//RFI
															array_push($responseArray,array("method" => "GET","flawtype" => "RFI","line" => $next_line,"totalline" => 1,"linecontent" => $line,"path" => $this->prepare_project_path_to_show($projectpath."/".$readitem)));	
														}else{//LFI
															array_push($responseArray,array("method" => "GET","flawtype" => "LFI","line" => $next_line,"totalline" => 1,"linecontent" => $line,"path" => $this->prepare_project_path_to_show($projectpath."/".$readitem)));
														}
														break;
													}
												}
											}
										}
									}
									//******FI-end*****
									//******CI*****
									$varsGET4CmdExec=$this->findAssignedVar($line,$varsGET4CmdExec,'CmdExec');
									if(false!==stripos($line,'exec') || false!==stripos($line,'shell_exec')  || false!==stripos($line,'system') || false!==stripos($line,'passthru') || false!==stripos($line,'proc_open') || false!==stripos($line,'popen')   ){
										if(false !== ($MultipleLines=$this->isMultipleLine($fileArray, $line)) ){// if there is a command line consisting of multiple row.
											foreach ($varsGET4CmdExec as $index => $value) {
												foreach ($MultipleLines as $MLindex => $MLvalue) {
													$linecontent4GETCmdExec.=$this->removeUnneccessarySpaces($MLvalue, $MLindex);
													if(false===strpos($line,'escapeshellarg') && false===strpos($line,'escapeshellcmd') && false===strpos($line,'array_filter') && (false===strpos($line,'filter_var') && false===strpos($line,'FILTER_CALLBACK') ) ){
														if($this->compare($MLvalue,$value)){
															array_push($responseArray,array("method" => "GET","flawtype" => "XSS","line" => $next_line,"totalline" => $MLindex+1,"linecontent" => $linecontent4GETCmdExec ,"path" => $this->prepare_project_path_to_show($projectpath."/".$readitem)));
															$linecontent4GETCmdExec='';
															break;//leave the if statements and also the foreach loop //keep in mind that "break 2;" leaves two loop
														}
										
													}
												}
												$linecontent4GETCmdExec='';
											}
										}else{// if there is a command line consisting of only one row.
											foreach($varsGET4CmdExec as $index => $value){
												if($this->compare($line,$value)){
													if(false===strpos($line,'escapeshellarg') && false===strpos($line,'escapeshellcmd') && false===strpos($line,'array_filter') && (false===strpos($line,'filter_var') && false===strpos($line,'FILTER_CALLBACK') ) ){
														array_push($responseArray,array("method" => "GET","flawtype" => "CI","line" => $next_line,"totalline" => 1,"linecontent" => $line,"path" => $this->prepare_project_path_to_show($projectpath."/".$readitem)));
														break;
													}
														
												}
											}
										}
									}
									//******CI-end*****
								}
						}
						//-----------------------------------------------------------
					}
				}	
			}
			closedir($diropened);
			return $responseArray;
		}
		function isRFI($line){
			$line=strstr($line,'$',true);// gets a substring backwards from first "$" character
			while((substr($line, -1, 1)=='\"') || (substr($line, -1, 1)=="\'") || (substr($line, -1, 1)=='(') ){//deletes \" , \' and ( character
				$line=substr($line, 0, -1);
			}
			$chars=str_split($line);
			foreach ($chars as $key => $val){
				if(ord($val)!= 32){// if not "space" character
					$remaining.=$val;
				}
			}
			if($remaining=='include' || $remaining=='include_once' || $remaining=='require' || $remaining=='require_once'){// this vulnerability is RFI
				return true;
			}
			return false;// Otherwise this is LFI
		}
		function compare($line,$value){
			$line=strstr($line,$value);
			$chars=str_split($line);
			foreach ($chars as $key => $val){
				if($key!=0){
					if(!(ord($val) <= 90 && ord($val) >= 65) && !(ord($val) <= 122 && ord($val) >= 97) && (ord($val) != 95) && !(ord($val) <= 57 && ord($val) >=48) ){
						$varName=substr($line,0,($key));// gets var name with "$"
						break;
					}
				}
			}
			if($varName==$value){
				 return true;
			}
			return false;
		}
		function isMultipleLine($fileArr, $comeLine){
			$multipleLinesArr=array();
			foreach ($fileArr as $key => $line) {
				$comeLine==$line?$state=true:null;
				$untouchedline=$line;
				if($state){
					foreach (array("#","//","/*") as $value) {
						if(false !== strpos($line,$value)){
							$line=strstr($line,$value,true);// gets substr backwards
						}
					}
					
					if(false !== ($lastSemicolonPos=strrpos($line,';'))){
						while(1){
							if($lastSemicolonPos == ($firstSemicolonPos=strpos($line,';'))){
								$str4char=$line;
								break;
							}else{
								$line=substr($line,$firstSemicolonPos+1);
								$line=strstr($line,';');
								//echo "line:".$line."\n";
								$lastSemicolonPos=strrpos($line,';');
							}
						}
						
					}else{
						array_push($multipleLinesArr,$untouchedline);
						continue;
					}
					/*$chars=str_split($str4char);
					foreach ($chars as $key => $val){
						if($key!=0){
							if(ord($val) != 32 ){
								echo "line:".$line."\n";
								array_push($multipleLinesArr,$untouchedline);
								continue 2;// the secondary parent loop continues.
							}
						}
					}*/
					if($comeLine == $untouchedline)return false;
					else {
						array_push($multipleLinesArr,$untouchedline);
						break;
					}
				}
			}
			//print_r($multipleLinesArr);
			return $multipleLinesArr;
		}
		
		function removeUnneccessarySpaces($MLvalue, $MLindex) {
			$oneSpace=false;$dontwrite=false;
			foreach (str_split($MLvalue) as $k => $v) {
				if($oneSpace==true && ord($v)==32){
					$dontwrite=true;
				}
				if(ord($v)==32){
					$oneSpace=true;
				}else{
					$dontwrite=false;
					$oneSpace=false;
				}
				if(ord($v)!=10 && ord($v)!=9 && ord($v)!=13){
					if(!$dontwrite){
						if($MLindex!=0 && $k==0){
						}else{
							$str.=$v;
						}
					}elseif($MLindex == 0){
						$str.=$v;
					}
				}
			}
			return $str;
		}
		
		
		function findAssignedVar($line,$arr,$whoCalled){
			foreach ($arr as $index => $val){
				if($whoCalled=='SQLi'){
					if(false!==strpos($line,$val) && false!==strpos($line,'=')){// assignment exists
						if(false===strpos($line,'filter_var') && false===strpos($line,'filter_var_array') && false===strpos($line,'array_filter') && (false===strpos($line,'filter_var') && false===strpos($line,'FILTER_CALLBACK') ) ){
							$var=stristr($line , '=' , true);//gets substr backwards from "=" in the line
							if(substr($var, -1, 1)=='.' || substr($var, -1, 1)=='-' || substr($var, -1, 1)=='+' || substr($var, -1, 1)=='*' || substr($var, -1, 1)=='/' || substr($var, -1, 1)=='%' || substr($var, -1, 1)=='&' || substr($var, -1, 1)=='|' || substr($var, -1, 1)=='^'){
								if(substr($var, -1, 2)=='**' || substr($var, -1, 2)=='<<' || substr($var, -1, 2)=='>>' ){
									$var=substr($var, 0, -2);
								}else{
									$var=substr($var, 0, -1);
								}
							}
							while(substr($var, -1, 1)==' '){//deletes suffix spaces character
								$var=substr($var, 0, -1);
							}
							$var=substr($var, strpos($var,'$'));//deletes prefix spaces character
							//------------checking its existence, then adding to the array---------------
							$exists=false;
							foreach ($arr as $indx => $vl){
								if($vl==$var){
									$exists=true;
								}
							}
							if($exists===false){
								array_push($arr,$var);
							}
							//-------------end of checking its existence-------------------------------
						}else{//the coming line involves a sanitizing method
							$before=stristr($line , '=' , true);//gets substr backwards from "=" in the line
							if(substr($before, -1, 1)=='.' || substr($before, -1, 1)=='-' || substr($before, -1, 1)=='+' || substr($before, -1, 1)=='*' || substr($before, -1, 1)=='/' || substr($before, -1, 1)=='%' || substr($before, -1, 1)=='&' || substr($before, -1, 1)=='|' || substr($before, -1, 1)=='^'){
								if(substr($before, -1, 2)=='**' || substr($before, -1, 2)=='<<' || substr($before, -1, 2)=='>>' ){
									$before=substr($before, 0, -2);
								}else{
									$before=substr($before, 0, -1);
								}
							}
							while(substr($before, -1, 1)==' '){//deletes suffix spaces character
								$before=substr($before, 0, -1);
							}
							$before=substr($before, strpos($before,'$'));//deletes prefix spaces character
							$after=stristr($line , '=');//gets substr forwards from "=" in the line including "=".
							$after=stristr($after,'$');//gets substr forwards from "$" in the line including "$".
							if(false !== strpos($after,$before)){// the unsanitized variable on the right side is sanitized and then assigned to itself, so it's no more a malicious varriable. We have to delete it from the malicious array
								foreach ($arr as $key => $v){
									if($v==$before){
										unset($arr[$key]);
									}
								}
							}
						}
					}		
				}
				if($whoCalled=='XSS'){
					if(false!==strpos($line,$val) && false!==strpos($line,'=')){
						if(false===strpos($line,'filter_var') && false===strpos($line,'filter_var_array') && false===strpos($line,'htmlspecialchars') && false===strpos($line,'htmlentities') && false===strpos($line,'highlight_string') &&  false === strpos($line,'strip_tags') && false===strpos($line,'array_filter') && (false===strpos($line,'filter_var') && false===strpos($line,'FILTER_CALLBACK') ) ){
							$var=stristr($line , '=' , true);//gets substr backwards from "=" in the line
							if(substr($var, -1, 1)=='.' || substr($var, -1, 1)=='-' || substr($var, -1, 1)=='+' || substr($var, -1, 1)=='*' || substr($var, -1, 1)=='/' || substr($var, -1, 1)=='%' || substr($var, -1, 1)=='&' || substr($var, -1, 1)=='|' || substr($var, -1, 1)=='^'){
								if(substr($var, -1, 2)=='**' || substr($var, -1, 2)=='<<' || substr($var, -1, 2)=='>>' ){
									$var=substr($var, 0, -2);
								}else{
									$var=substr($var, 0, -1);
								}
							}
							while(substr($var, -1, 1)==' '){//deletes suffix spaces character
								$var=substr($var, 0, -1);
							}
							$var=substr($var, strpos($var,'$'));//deletes prefix spaces character
							//------------checking its existence, then adding to the array---------------
							$exists=false;
							foreach ($arr as $indx => $vl){
								if($vl==$var){
									$exists=true;
								}
							}
							if($exists===false){
								array_push($arr,$var);
							}
							//-------------end of checking its existence-------------------------------
						}else{//the coming line involves a sanitizing method
							$before=stristr($line , '=' , true);//gets substr backwards from "=" in the line
							if(substr($before, -1, 1)=='.' || substr($before, -1, 1)=='-' || substr($before, -1, 1)=='+' || substr($before, -1, 1)=='*' || substr($before, -1, 1)=='/' || substr($before, -1, 1)=='%' || substr($before, -1, 1)=='&' || substr($before, -1, 1)=='|' || substr($before, -1, 1)=='^'){
								if(substr($before, -1, 2)=='**' || substr($before, -1, 2)=='<<' || substr($before, -1, 2)=='>>' ){
									$before=substr($before, 0, -2);
								}else{
									$before=substr($before, 0, -1);
								}
							}
							while(substr($before, -1, 1)==' '){//deletes suffix spaces character
								$before=substr($before, 0, -1);
							}
							$before=substr($before, strpos($before,'$'));//deletes prefix spaces character
							$after=stristr($line , '=');//gets substr forwards from "=" in the line including "=".
							$after=stristr($after,'$');//gets substr forwards from "$" in the line including "$".
							if(false !== strpos($after,$before)){// the unsanitized variable on the right side is sanitized and then assigned to itself, so it's no more a malicious varriable. We have to delete it from the malicious array
								foreach ($arr as $key => $v){
									if($v==$before){
										unset($arr[$key]);
									}
								}
							}
						}
					}	
				}
				if($whoCalled=='CmdExec'){
					if(false!==strpos($line,$val) && false!==strpos($line,'=')){
						if(false===strpos($line,'escapeshellarg') && false===strpos($line,'escapeshellcmd') && false===strpos($line,'array_filter') && (false===strpos($line,'filter_var') && false===strpos($line,'FILTER_CALLBACK') ) ){
							$var=stristr($line , '=' , true);//gets substr backwards from "=" in the line
							if(substr($var, -1, 1)=='.' || substr($var, -1, 1)=='-' || substr($var, -1, 1)=='+' || substr($var, -1, 1)=='*' || substr($var, -1, 1)=='/' || substr($var, -1, 1)=='%' || substr($var, -1, 1)=='&' || substr($var, -1, 1)=='|' || substr($var, -1, 1)=='^'){
								if(substr($var, -1, 2)=='**' || substr($var, -1, 2)=='<<' || substr($var, -1, 2)=='>>' ){
									$var=substr($var, 0, -2);
								}else{
									$var=substr($var, 0, -1);
								}
							}
							while(substr($var, -1, 1)==' '){//deletes suffix spaces character
								$var=substr($var, 0, -1);
							}
							$var=substr($var, strpos($var,'$'));//deletes prefix spaces character
							//------------checking its existence, then adding to the array---------------
							$exists=false;
							foreach ($arr as $indx => $vl){
								if($vl==$var){
									$exists=true;
								}
							}
							if($exists===false){
								array_push($arr,$var);
							}
							//-------------end of checking its existence-------------------------------
						}else{//the coming line involves a sanitizing method
							$before=stristr($line , '=' , true);//gets substr backwards from "=" in the line
							if(substr($before, -1, 1)=='.' || substr($before, -1, 1)=='-' || substr($before, -1, 1)=='+' || substr($before, -1, 1)=='*' || substr($before, -1, 1)=='/' || substr($before, -1, 1)=='%' || substr($before, -1, 1)=='&' || substr($before, -1, 1)=='|' || substr($before, -1, 1)=='^'){
								if(substr($before, -1, 2)=='**' || substr($before, -1, 2)=='<<' || substr($before, -1, 2)=='>>' ){
									$before=substr($before, 0, -2);
								}else{
									$before=substr($before, 0, -1);
								}
							}
							while(substr($before, -1, 1)==' '){//deletes suffix spaces character
								$before=substr($before, 0, -1);
							}
							$before=substr($before, strpos($before,'$'));//deletes prefix spaces character
							$after=stristr($line , '=');//gets substr forwards from "=" in the line including "=".
							$after=stristr($after,'$');//gets substr forwards from "$" in the line including "$".
							if(false !== strpos($after,$before)){// the unsanitized variable on the right side is sanitized and then assigned to itself, so it's no more a malicious varriable. We have to delete it from the malicious array
								foreach ($arr as $key => $v){
									if($v==$before){
										unset($arr[$key]);
									}
								}
							}
						}
					}
				}
				if($whoCalled=='FI'){
					if(false!==strpos($line,$val) && false!==strpos($line,'=')){
						if( (false===strpos($line,'filter_var') && false===strpos($line,'FILTER_CALLBACK') ) && ( false===strpos($line,'filter_var_array') && false===strpos($line,'FILTER_CALLBACK') ) && ( false===strpos($line,'explode') && false===strpos($line,'"/') ) && false===strpos($line,'array_filter') ){
							$var=stristr($line , '=' , true);//gets substr backwards from "=" in the line
							if(substr($var, -1, 1)=='.' || substr($var, -1, 1)=='-' || substr($var, -1, 1)=='+' || substr($var, -1, 1)=='*' || substr($var, -1, 1)=='/' || substr($var, -1, 1)=='%' || substr($var, -1, 1)=='&' || substr($var, -1, 1)=='|' || substr($var, -1, 1)=='^'){
								if(substr($var, -1, 2)=='**' || substr($var, -1, 2)=='<<' || substr($var, -1, 2)=='>>' ){
									$var=substr($var, 0, -2);
								}else{
									$var=substr($var, 0, -1);
								}
							}
							while(substr($var, -1, 1)==' '){//deletes suffix spaces character
								$var=substr($var, 0, -1);
							}
							$var=substr($var, strpos($var,'$'));//deletes prefix spaces character
							//------------checking its existence, then adding to the array---------------
							$exists=false;
							foreach ($arr as $indx => $vl){
								if($vl==$var){
									$exists=true;
								}
							}
							if($exists===false){
								array_push($arr,$var);
							}
							//-------------end of checking its existence-------------------------------
						}else{//the coming line involves a sanitizing method
							$before=stristr($line , '=' , true);//gets substr backwards from "=" in the line
							if(substr($before, -1, 1)=='.' || substr($before, -1, 1)=='-' || substr($before, -1, 1)=='+' || substr($before, -1, 1)=='*' || substr($before, -1, 1)=='/' || substr($before, -1, 1)=='%' || substr($before, -1, 1)=='&' || substr($before, -1, 1)=='|' || substr($before, -1, 1)=='^'){
								if(substr($before, -1, 2)=='**' || substr($before, -1, 2)=='<<' || substr($before, -1, 2)=='>>' ){
									$before=substr($before, 0, -2);
								}else{
									$before=substr($before, 0, -1);
								}
							}
							while(substr($before, -1, 1)==' '){//deletes suffix spaces character
								$before=substr($before, 0, -1);
							}
							$before=substr($before, strpos($before,'$'));//deletes prefix spaces character
							$after=stristr($line , '=');//gets substr forwards from "=" in the line including "=".
							$after=stristr($after,'$');//gets substr forwards from "$" in the line including "$".
							if(false !== strpos($after,$before)){// the unsanitized variable on the right side is sanitized and then assigned to itself, so it's no more a malicious varriable. We have to delete it from the malicious array
								foreach ($arr as $key => $v){
									if($v==$before){
										unset($arr[$key]);
									}
								}
							}
						}
					}
				}
				
			}
			return $arr;
		}
		
	}
	
?>
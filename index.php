<!DOCTYPE html>
<html lang="en">
    
    <head>
        <meta charset=utf-8>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>SCA</title>
        <!-- Load Roboto font -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <!-- Load css styles -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" type="text/css" href="css/pluton.css" />
        <!--[if IE 7]>
            <link rel="stylesheet" type="text/css" href="css/pluton-ie7.css" />
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="css/jquery.cslider.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" />
        <link rel="stylesheet" type="text/css" href="css/animate.css" />
        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/apple-touch-icon-72.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57.png">
        <link rel="shortcut icon" href="images/ico/favicon.ico">
		<?php 
		@include("./php/session_class.php");//involves session processes
		@include("./php/file_class.php");//comprises file processes
        @new sessionClass();
        sessionClass::check_and_setCookie();
		?>
    </head>
    
    <body>
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container">
                    <a href="#" class="nal">
                        <img src="images/scalogo.png" width="60" height="40" alt="Logo" /><!-- width="60" height="40" -->
                        <!-- This is website logo -->
                    </a>
                    <!-- Navigation button, visible on small resolution -->
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <i class="icon-menu"></i>
                    </button>
                    <!-- Main navigation -->
                    <div class="nav-collapse collapse pull-right">
                        <ul class="nav" id="top-navigation">
                            <li class="active"><a href="#home">Home</a></li>
                            <li><a href="#service">Services</a></li>
                            <li><a href="#analysis">Analysis</a></li>
                            <li><a href="#report">Security Report</a></li>
                            <li><a href="#download">Download</a></li> 
                            <li><a href="#contact">Contact</a></li> 
                        </ul>
                    </div>
                    <!-- End main navigation -->
                </div>
            </div>
        </div>
        <!-- Start home section -->
		<div class="section secondary-section" id="home">
            <div class="triangle"></div>
            <div class="container centered">
                <p class="large-text">Elegance is not the abundance of simplicity. It is the absence of complexity.<br>The elegant software SCA4PHP wellcomes you.</p>
               <!-- <a href="#" class="button">Purshase now</a>-->
            </div>
        </div>
        <!-- End home section -->
        <!-- Service section start -->
        <div class="section primary-section" id="service">
		 <div class="triangle"></div>
            <div class="container">
                <!-- Start title section -->
               
                <div class="row-fluid">
                    <div class="span4">
                        <div class="centered service">
						  <a href="#analysis" >
                            <div class="circle-border zoom-in">
                               <img class="img-circle" src="images/Service2.png" alt="service 1">
                            </div>
                            <h3>Start Analysis</h3> 
						 </a>
                            <p>No Web Application Insecure Until It is Exploited/Hacked.</p>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="centered service">
						  <a href="#service" >
                            <div class="circle-border zoom-in">
                                <img class="img-circle" src="images/Service1.png" alt="service 2" />
                            </div>
                            <h3>About The Software</h3>
						  </a>
                            <p>We Love Finding Vulnerabilities and Flaws Of PHP Web Applications.<br>Here is Our Work.<br><i>SCA "Static Code Analyser"</i><br>Just Let It Analyse.</p>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="centered service">
						  <a href="#projects-title" >
                            <div class="circle-border zoom-in">
                                <img class="img-circle" src="images/Service3.png" alt="service 3">
                            </div>
                            <h3>Projects</h3>
						  </a>
                            <p>We provide you with multiple project to analyse, inspect and download simultaneously.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Service section end -->

        <!-- Analysis section start -->
        <div class="section secondary-section" id="analysis">
            <div class="triangle"></div>
            <div class="container">
                    <div class="row title">
                        <h1>How it Works?</h1>
                        <p>1. Select code &nbsp;&nbsp; 2. Static Code Analysis &nbsp;&nbsp;3. Security Report</p>
                    </div>
                    <div class="row title">
                    	<div id="uploadingDiv" class="span6 offset3">
	                    	<form name="yukleme" method="post" action="#analysis" enctype="multipart/form-data">
	                            <p id="">Upload your PHP web project as zip file: </p>
	                            <input type="file" class="button" name="fileupload" accept=".zip"><!-- "multiple" is attribute of the input "file" for multiple choice -->
	                            <br><br><input type="submit" class="button" id="load" name="load" value="Load">
                      	    </form> 
                      	    <input type="hidden" id="isLoadClicked" value="No">
	                    </div>
	                    <div id='informingDiv' class="span5">
	                    	<?php
					            $list=array();
					            $list=File::extract_zip_to_projects();
			                ?>
		                    <p id="infoP"><?php
				                    echo($list["result"]);
			              	?></p>
	                   		<?php
			                    if(File::get_all_my_projects('./projects') != null){
			                    	echo '<button id="startAnalysis" class="button">Start Analysis</button>';
			                    	echo '<div><label id="lbwarning" style="color : blue"></label></div><p>Select your project to analize<br><br>';
			                    	foreach ((File::get_all_my_projects('./projects')) as $key => $value){
			                    		echo '<input type="checkbox" class="projectItem" id="projectItem'.$key.'" value="'.sessionClass::remove_addition_name($value).'">&nbsp;'.sessionClass::remove_addition_name($value).'&nbsp;&nbsp;';
			                    		$lenght+=1;
			                    	}
			                    	echo '<label id="count" style="display:none">'.$lenght.'</label>';
			                    	echo '</p>';
			                    }
		                    ?>
	                    </div>
                    </div>
                    <div id="projects-row">
						<h3 id="projects-title">Projects</h3>
						<div class="row-fluid">
							<div id="file-viewer" class="span3 single-project animated pulse">
								<?php 
	                            //if($list["status"] == true ){ // $list["status"]== true" means that if there is a value of the key 'status' of the array 'list'. But === true means logically 1 value. keep in mind that.
	                                echo File::create_tree("./projects");
	                            //}
								?>
							</div>
	                        <div id="file-content-container" class="span8 single-project">
	                            <div id="file-content-caption" class="secondary-section" ></div>
	                            <div id="file-content" class=""></div>
	                        </div>
							
						</div>
                    </div>
            </div>
        </div>
        <!-- Analysis section end -->
        <!-- Report section start -->
    	<div class="section primary-section" id="report">
            <div class="triangle"></div>
            <div id="reportContainer" class="container">
                <div class="title">
                    <h1>Security Report</h1>
                    <p>Select your project to see its security report.</p>
                </div>
                <div id="showProjects"></div>
                <div id="showProjectsReportDetails"></div> 
            </div>
            <div id="inspection">
	            <div class="section primary-section">
	                <div class="triangle"></div>
	                <div class="container">
	                    <div class="title">
	                        <h1 id="projectNameCaption"></h1>
	                        <p>To see it on code block, click on its image</p>
	                    </div>
	                    <div id="vulnerabilitiesContainer" class="row"></div> 
	                    <p class="testimonial-text">
	                        "Web applications are not insecure, till they are exploited/hacked"
	                    </p>
	                </div>
	            </div>
	       </div>
        </div>
        <!-- Report section end -->
        <!-- Download section start -->
        <div class="section third-section" id="download">
            <div class="container centered">
                <div class="sub-section">
                    <div class="title clearfix">
                        <div class="pull-left">
                            <h3>Downloads</h3>
                        </div>
                    </div>
                    <div id="downloadItemContainer">
                    </div>
                </div>
            </div>
        </div>
        <!-- Download section end -->
        <!-- Contact section start -->
        <div id="contact" class="contact">
            <div class="section secondary-section">
            	<div class="triangle"></div>
                <div class="container">
                    <div class="title">
                        <h1>Contact Us</h1>
                        <p>Please, do not hesitate to contact us for anything.</p>
                    </div>
                </div>
                <div class="container">
                    <div class="span9 center contact-info">
                        <p class="info-mail">talhasen1@gmail.com</p>
                        <div class="title">
                            <h3>We Are Social</h3>
                        </div>
                    </div>
                    <div class="row-fluid centered">
                        <ul class="social">
                            <li>
                                <a href="">
                                    <span class="icon-facebook-circled"></span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="icon-twitter-circled"></span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="icon-linkedin-circled"></span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="icon-pinterest-circled"></span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="icon-dribbble-circled"></span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="icon-gplus-circled"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Contact section end -->
        <!-- Footer section start -->
        <div class="footer">
            <p>&copy; 2013 Theme by <a href="http://www.graphberry.com">GraphBerry</a>, <a href="http://goo.gl/NM84K2">Documentation</a></p>
        </div>
        <!-- Footer section end -->
        <!-- ScrollUp button start -->
        <div class="scrollup">
            <a href="#">
                <i class="icon-up-open"></i>
            </a>
        </div>
        <!-- ScrollUp button end -->
        <!-- Include javascript -->
        <script src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.mixitup.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="js/jquery.cslider.js"></script>
        <script type="text/javascript" src="js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="js/jquery.inview.js"></script>
        <!-- Load google maps api and call initializeMap function defined in app.js -->
        <script async="" defer="" type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&callback=initializeMap"></script>
        <!-- css3-mediaqueries.js for IE8 or older -->
        <!--[if lt IE 9]>
            <script src="js/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="js/app.js"></script>
		<script type="text/javascript">
			$(function(){
				$('.unorderedlist').toggle();
				$('.treeview').find('SPAN').click(function(e){
					$(this).parent().children('UL').toggle();
				});
				$('#load').mousedown(function(e){
					$('#isLoadClicked').attr("value","Yes");
				});
				$('.projectItem').mousemove(function(e){
					for(i=0;i<document.getElementById('count').innerText;i++){
						if(document.getElementById('projectItem'+i).checked){
							$('#lbwarning').css("display","none");
						}
					}
				});
				$(window).load(function() {
					if($('#infoP').html() != ''){
						$("#uploadingDiv").attr("class","span6");
						$("#informingDiv").css("display","block");
					}else{
						$("#uploadingDiv").attr("class","span6 offset3");
						$("#informingDiv").css("display","none");
					}
					if($('.projectItem').html() != null){
							$("#projects-row").css("display","block");
					}
				});
				
				$('#startAnalysis').click(function(){
					if($(this).parent().find('input')!=null){// to avoid malicious users
						var noOneChecked=true;
						var dataToPost={};
						for(i=0;i<document.getElementById('count').innerText;i++){
							var element=document.getElementById('projectItem'+i);
							if(element.checked){
								noOneChecked=false;
								dataToPost[i]=element.value;
							}
						}
						if(!noOneChecked){
							$("#lbwarning").css("display","none");
							$.ajax({
	                            url: "./php/start_analysis.php",
	                            type: "POST",
	                            data: dataToPost,
	                            dataType: "json",// the type of result variable below
	                            success: function(result){
		                            dataToPost["analysis"]=result;
		                            //--------start pdf saving------
		                            $.ajax({
			                            url: "./php/make_pdf.php",
			                            type:"POST",
			                            data: dataToPost
				                    }).done(function(responsecoming){
										//when it is done, here works. Code here if needed
				                    });
		                            //-------end pdf saving------
	                            	$("#reportContainer").css("display","block");
	                            	$("#download").css("display","block");
		                            var rowsOfProjects='';
		                            var AllProjectReports='';
		                            for(i=0; i<((result.length)/3);i++){
										rowsOfProjects+='<div class="row-fluid team">';
										for(j=0; j<result.length; j++){
											if(j>=(i*3) && j<((i+1)*3)){
												rowsOfProjects+='<div class="span4" id="ProjectReport'+(j+1)+'">'
							                        				+'<div class="thumbnail">'
					                            						+'<img src="images/report.png" alt="team 1">'
					                           						 	+'<h3>'+result[j][0]["project_name"].toUpperCase()+'</h3>'
					                           						 	+'<div class="mask">'
					                	                                	+'<a href="#report"><h2 onclick="bringReportDetails(this)">'+result[j][0]["project_name"].toUpperCase()+'</h2></a>'
					                	                                	+'<p>Click on the project name above to show its report.</p>'
					                	                            	+'</div>'
					                	                            +'</div>'
					                	                            +'<div id="download'+(j+1)+'" style="width:70px;height:70px; margin:0 auto" class="download">'
							                	                            +'<a href="#download" >'
										                                    	+'<img src="images/Downloads-Black-Folder-icon.png" alt="Download '+result[j][0]["project_name"].toUpperCase()+'">'
										                                	+'</a>'
									                                	+'</div><br>'	
					                	                        +'</div>';	
											}
										} 	
										rowsOfProjects+='</div><br>';
		                            }
		                            $("#showProjects").html(rowsOfProjects);
		                            $('html, body').animate({
		                                scrollTop: $("#reportContainer").offset().top-103
		                            }, 1000);
		                            //document.getElementById('reportContainer').scrollIntoView();
		                            var downloadItems='';
		                            for(i=0; i<result.length; i++){
		                            	//console.log(result);
		                            	//console.log(Object.keys(result[i][0]).length);
		                            	//alert(JSON.stringify(result[i][0]["project_name"]));
		                            	for(j=0; j<=(Object.keys(result[i][0]).length)-2; j++){
						                	        AllProjectReports+='<div id="vulnerabilityOf'+result[i][0]["project_name"]+'" class="span4" style="display:none">'
													                        +'<div class="testimonial">'
												                                +'<p>There is an un sanitised variable which leads to '+result[i][0][j]["flawtype"]+' vulnerability.</p>'
													                                +'<div id="code'+j+'" class="codeViewBlock" style="overflow:scroll; height:100px; background-color:rgba(255, 255, 255, 1); width:370px; display:none">'//rgba(255, 255, 255, 1)//#FECE1A
													                                +'</div>'
													                            +'<p>Click on the image below to see the code line.</p>'
												                                +'<div class="whopic">'
												                                    +'<div class="arrow"></div>'
												                                    +'<img src="images/ico/'+result[i][0][j]["flawtype"]+'.jpg" class="centered" alt="'+result[i][0][j]["flawtype"]+'" onclick="showVulnerabilityLine(this,'+j+','+result[i][0][j]["line"]+','+result[i][0][j]["totalline"]+')">'
												                                    +'<strong>on line '+result[i][0][j]["line"]
												                                        +'<small>in <div id="path">'+result[i][0][j]["path"]+'</div></small>'
												                                    +'</strong>'
												                                +'</div>'
												                            +'</div>'
												                        +'</div>';
							                       	
		                            	}
		                            	downloadItems+='<div id="downloadItem'+(i+1)+'" class="span2">'
		                            							 //+'<a href="./reports/'+result[i][0]["project_name"]+'.pdf">'
		                            							 +'<a href="" download>'
		                            							 	+'<h6 style="text-align:center">'+result[i][0]["project_name"].toUpperCase()+'</h4>'
							                                    	+'<img src="images/Downloads-Black-Folder-icon.png" alt="client logo">'
						                                		+'</a>'
						                            	+'</div>';
		                            			
		                            }
		                            $('#downloadItemContainer').html(downloadItems);
			                        
		                           	$.ajax({//
	  			                        url: "./php/get_my_reports_root_dir.php",
	  			                        type:"POST",
	  			                        data:result,
	  			                        dataType:"text",
	  			                        success:function(dir_name){
	  			                           for(i=0; i<result.length; i++){
	  			                        	  var nxt=i+1;
		  			                          $("#downloadItem"+nxt).children("a").attr("href","reports/"+dir_name+'/'+result[i][0]["project_name"]+".pdf");
		  			                          $("#downloadItem"+nxt).children("a").attr("download",result[i][0]["project_name"]+".pdf");
	  			                           }
		  			                    }
	  				                });
		                            
		                            $("#vulnerabilitiesContainer").html(AllProjectReports);
	                                
	                            }
	                        }).fail(function(jqXHR, textStatus, errorThrown){
	                             alert("textStatus:"+textStatus+" --- errorThrown:"+errorThrown);
	                        });
						}else{
							$("#lbwarning").css("display","block");
							$("#lbwarning").html('<br>No one selected. Please select a project or a few project below!');
						}
						/**/
					}
					
				});
                $('.treeview').find('LI').dblclick(function(e){
                    //find_path($(this).parent().parent());
                    //alert($(this));
                    if($(this).attr("class")=="file"){
                            var dataToPost={
                                "filename":$(this).children("SPAN").text(),
                                "filepath":$(this).children('SPAN').attr("class")
                            };
                            $.ajax({
                                url: "./php/codeview.php",
                                type: "POST",
                                data: dataToPost,
                                dataType: "text",// the type of result variable below
                                success: function(result){
                                    $("#file-content").html(result);
                                    $('#file-content').find('code').attr('id','code-block-content-code-item');
                                    // $('#file-content').find('span').attr('class','code-line');
                                    
                                    $('#file-content-caption').append('<button class="button-ps" id="'+dataToPost["filepath"]+'">'+dataToPost["filename"]+'</button>');
                                    $('#file-content-caption').append('<button class="button-ps menu-button" id="'+dataToPost["filepath"]+'"></button>&nbsp;');
                                    $('#file-content-caption').children('button').attr("onclick","showfile(this)");
                                }
                            }).fail(function(jqXHR, textStatus, errorThrown){
                                 alert("textStatus:"+textStatus+" --- errorThrown:"+errorThrown);
                            });
                    }
                });
			});
			function bringReportDetails(e){
					$("#projectNameCaption").text(e.innerText.toUpperCase());
					$("#inspection").css("display","block");
					$("#vulnerabilitiesContainer").children("div").css("display","none");
					$("#vulnerabilitiesContainer").children("div#vulnerabilityOf"+e.innerText.toLowerCase()).css("display","block");
					$('html, body').animate({
                        scrollTop: $("#inspection").offset().top-103
                    }, 1000);
					//projelerin hepsini yaz ama display:none ile yaz
	        }
            function showfile(menuItem){
                if($(menuItem).text()!=''){//file button
	                var dataToPost={
	                    "filename":$(menuItem).text(),
	                    "filepath":$(menuItem).attr("id")
	                };
	                //alert(JSON.stringify(dataToPost));
	                $.ajax({
	                    url: "./php/codeview.php",
	                    type: "POST",
	                    data: dataToPost,
	                    dataType: "text",// the type of result variable below
	                    success: function(result){
	                        $("#file-content").html(result);
	                        $('#file-content').find('code').attr('class','codeview');                            
	                    }
	                }).fail(function(jqXHR, textStatus, errorThrown){
	                    alert("textStatus:"+textStatus+" --- errorThrown:"+errorThrown);
	                });
                }else{//exit button
                    var id=$(menuItem).attr("id");
                	var Item=document.getElementById(id);
                	Item.remove();
					$(menuItem).remove();
					//here find last click menuitem or anyone and prepare an ajax request to get content of the file and render it to #file-content element as below
					$("#file-content").html('<hr class="codeViewHrFirst"><ul id="codeUL" class="treeview"><li class="file"></li><hr class="codeViewHr"><li class="codeLI"></li></ul>');
                }
            }
            function showVulnerabilityLine(e,j,lineNo,totalline){
                var path=$(e).parent().find('div#path').text();
				$(e).parent().parent().children("div#code"+j).css("display","block");
				var dataToPost={
						"linenumber":lineNo,
	                    "filepath":path,
	                    "totalline":totalline
				} 
				 $.ajax({
	                    url: "./php/codeview.php",
	                    type: "POST",
	                    data: dataToPost,
	                    dataType: "text",// the type of result variable below
	                    success: function(result){
	                        $("#code"+j).html(result);
	                        $("#code"+j).find('tr').children('td').next().children('code:first-child').next().css("background-color","#ff4646");            
	                    }
	                }).fail(function(jqXHR, textStatus, errorThrown){
	                    alert("textStatus:"+textStatus+" --- errorThrown:"+errorThrown);
	                });
            }	
		</script>
        <script>
       	function closeIt()// while leaving page show this message below
        {
            var data={
            		"isLoadButton":$('#isLoadClicked').attr("value")
                    };
             $.ajax({
	             url: "./php/delete_all_while_leaving.php",
	             type: "POST",
	             dataType: "text",// the type of result variable below
	             data:data,
	             async:false,
	             success: function(result){
		                                  
	             }
	         }).fail(function(jqXHR, textStatus, errorThrown){
	             alert("textStatus:"+textStatus+" --- errorThrown:"+errorThrown);
	         });
             /*return "Any string value here forces a dialog box to \n" + 
         	"appear before closing the window.";*/
        	
        }
        window.onbeforeunload = closeIt;
        //------------------------
        </script>
    </body>
</html>